package com.example.leidyquinterobernal.carta_restaurante;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class mostrar extends AppCompatActivity {

    String matriz[][];
    ListView lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);
        try {
            Object[] objectArray = (Object[]) getIntent().getExtras().getSerializable("matriz");
            if(objectArray!=null){
                matriz = new String[objectArray.length][];
                for(int i=0;i<objectArray.length;i++){
                    matriz[i]=(String[]) objectArray[i];
                }
            }
            lista = (ListView) findViewById(R.id.lista);
            Adaptador a = new Adaptador(this, matriz);
            lista.setAdapter(a);
        } catch (Exception e) {
            Log.e("En la creación", e.getMessage());
        }
    }
}
