package com.example.leidyquinterobernal.carta_restaurante;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;


public class Adaptador extends BaseAdapter {
    private static LayoutInflater inflater;
    Context contexto;
    String [][] datos;
    int id;

    public Adaptador(Context contexto, String[][] datos){
        this.contexto = contexto;
        this.datos = datos;
        inflater = (LayoutInflater)contexto.getSystemService(contexto.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return datos.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final View  vista = inflater.inflate(R.layout.design,null);

        TextView titulo = (TextView) vista.findViewById(R.id.textView2);
        TextView descripcion = (TextView) vista.findViewById(R.id.textView3);
        ImageView thumb = (ImageView) vista.findViewById(R.id.imageView2);

        titulo.setText(datos[i][0]);
        descripcion.setText(datos[i][1]);
        Glide.with(contexto).load(datos[i][2]).into(thumb);
        return vista;
    }
    @Override
    public boolean isEnabled(int position)
    {
        return true;
    }

}
