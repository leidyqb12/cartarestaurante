package com.example.leidyquinterobernal.carta_restaurante;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    void mapa(View view){
        Intent mimapa = new Intent(this, maps.class);
        startActivity(mimapa);
    }
    void reservar(View view){
        Uri dir = Uri.parse("https://api.whatsapp.com/send?phone=573155167891");
        Intent pagina = new Intent(Intent.ACTION_VIEW, dir);
        startActivity(pagina);
    }
    void carta(View view){
        Intent micarta = new Intent(this, carta.class);
        startActivity(micarta);
    }
}
