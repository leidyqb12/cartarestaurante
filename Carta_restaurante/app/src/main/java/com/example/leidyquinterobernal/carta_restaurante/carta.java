package com.example.leidyquinterobernal.carta_restaurante;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class carta extends AppCompatActivity {

    String matriz [][];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);
    }
    void entrada(View view){
        matriz = new String[7][3];
        matriz[0][0] = "Morrones rellenos";
        matriz[0][1] = "Un plato en si mismo que puede ser vegetariano, apto para hacer también en la parrilla. $15.000";
        matriz[0][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201706/_W880_H495/617015.jpg";
        matriz[1][0] = "Papas rosti al horno";
        matriz[1][1] = "Crocantes y deliciosas, ideal para acompañar carnes. $10.000";
        matriz[1][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201705/_W880_H495/613298.jpg";
        matriz[2][0] = "Croquetas de acelga y choclo";
        matriz[2][1] = "Vegetarianas, son un acompañamiento buenísimo. $12.000";
        matriz[2][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201705/_W880_H495/610739.jpg";
        matriz[3][0] = "Tomates rellenos";
        matriz[3][1] = "Una delicia para los días más fríos, ideal para acompañar con papas al horno. $8.000";
        matriz[3][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201704/_W880_H495/608221.jpg";
        matriz[4][0] = "Quiche de pollo y choclo";
        matriz[4][1] = "Para comer como entrada, acompañado de ensalada. $14.000";
        matriz[4][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201703/_W880_H495/604517.jpg";
        matriz[5][0] = "Bocados de polenta";
        matriz[5][1] = "Ideal para bocados de copetín o para niños. $9.000";
        matriz[5][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201702/_W880_H495/601816.jpg";
        matriz[6][0] = "Croquetas de papa y queso";
        matriz[6][1] = "Con una ensalada de hojas verdes. $8.000";
        matriz[6][2] = "https://imagenes.montevideo.com.uy/imgnoticias/201407/_W880_H495/454866.jpg";
        siguiente();
    }
    void postre(View view){
        matriz = new String[7][3];
        matriz[0][0] = "Pie de Chocolate";
        matriz[0][1] = "¿Antojado de un postre delicioso? $16.000";
        matriz[0][2] = "https://encolombia.com/wp-content/uploads/2018/06/Receta-Pie-Chocolate.jpg";
        matriz[1][0] = "Mousse de Cafe";
        matriz[1][1] = "Para probar un delicioso postre no es necesaria una ocasión especial $12.000";
        matriz[1][2] = "https://encolombia.com/wp-content/uploads/2018/04/Mousse-Cafe-Receeta.jpg";
        matriz[2][0] = "Pastel de Cafe Frio";
        matriz[2][1] = "Ideal para pasar una tarde en compañía de familia o amigos $10.000";
        matriz[2][2] = "https://encolombia.com/wp-content/uploads/2018/02/Receta-Pastel-Cafe-Frio.jpg";
        matriz[3][0] = "Souffle de Chocolate";
        matriz[3][1] = "Este es un postre delicioso que no puedes dejar de probar, y lo puedes acompañar con una bola de helado de vainilla. $16.000";
        matriz[3][2] = "https://encolombia.com/wp-content/uploads/2017/12/Receta-Souffle-Chocolate.jpg";
        matriz[4][0] = "Tres Leches de Maracuyá";
        matriz[4][1] = "Porque un rico postre es ideal para subir el ánimo $12.000";
        matriz[4][2] = "https://encolombia.com/wp-content/uploads/2017/10/Receta-Postre-Tres-Leche-Maracuya.jpg";
        matriz[5][0] = "Tartaleta de Guayaba";
        matriz[5][1] = "¡Un delicioso postre siempre cae bien! $15.000";
        matriz[5][2] = "https://encolombia.com/wp-content/uploads/2017/09/Receta-Tartaleta-Guayaba.jpg";
        matriz[6][0] = "Rollo de Chocolate con Mora";
        matriz[6][1] = "Para ocasiones especiales, tiene la ventaja de poder reemplazar las moras por frutas fresas, arándanos o cerezas. $18.000";
        matriz[6][2] = "https://encolombia.com/wp-content/uploads/2013/08/rollo-de-chocolate-e1375810042845.jpg";
        siguiente();
    }
    void fuerte(View view){
        matriz = new String[7][3];
        matriz[0][0] = "Salmon";
        matriz[0][1] = "Filete de salmon servido con papa asada o papa rustica. Acompañadas de ensalada. $22.000";
        matriz[0][2] = "https://i1.wp.com/hatoviejo.com/prueba/wp-content/uploads/2014/11/Salmón_b.jpg?w=504";
        matriz[1][0] = "Punta de Anca";
        matriz[1][1] = "Nuestra deliciosa y jugosa Punta de Anca es servida con papa asada o papa rustica. Acompañadas de ensalada. $24.000";
        matriz[1][2] = "https://i0.wp.com/hatoviejo.com/prueba/wp-content/uploads/2014/11/Punta-de-Anca_b.jpg?w=504";
        matriz[2][0] = "Mixta con Langostinos";
        matriz[2][1] = "La Planchita mixta con langostinos trae grandes y frescos langostinos combinados con solomito, cañón de cerdo, pollo y pescado, servido con papa asada o papa rustica. Acompañadas de ensalada. $28.000";
        matriz[2][2] = "https://i1.wp.com/hatoviejo.com/prueba/wp-content/uploads/2014/07/Plancha-Mixta_b.jpg?w=504";
        matriz[3][0] = "Solomito Sterling";
        matriz[3][1] = "Nuestro Solomito Sterling es un corte grueso servido con papa asada o papa rustica. Acompañadas de ensalada. $20.000";
        matriz[3][2] = "https://i2.wp.com/hatoviejo.com/prueba/wp-content/uploads/2014/08/Sterling_Hatoviejo.jpg?w=504";
        matriz[4][0] = "Solomito";
        matriz[4][1] = "Nuestro solomito es servido con papa asada o papa rustica. Acompañadas de ensalada. $25.000";
        matriz[4][2] = "https://i2.wp.com/hatoviejo.com/prueba/wp-content/uploads/2014/08/Planchita-de-Solomito_Hatoviejo.jpg?w=504";
        matriz[5][0] = "Encocado de Pargo Frito";
        matriz[5][1] = "Pargo en salsa achiotada de pimentón, cebolla y leche de coco, servido con toston de platano, arroz con coco y acompañado de ensalada. $20.000";
        matriz[5][2] = "https://i1.wp.com/hatoviejo.com/prueba/wp-content/uploads/2014/07/Pargo-Frito2.jpg?w=504";
        matriz[6][0] = "Cerdo enTamarindo";
        matriz[6][1] = "Cerdo en Salsa de Tamarindo sobre yuca majada y rugula con tomates cherry $28.000";
        matriz[6][2] = "https://i2.wp.com/hatoviejo.com/prueba/wp-content/uploads/2017/06/Cerdo-en-salsa-de-tamarindo_Hatoviejo.jpg?w=504";
        siguiente();
    }
    void bebidas(View view){
        matriz = new String[7][3];
        matriz[0][0] = "Limonada de sandia";
        matriz[0][1] = "Refrescate al maximo con esta limonada de sandia. Acabara con tu sed en un dos por tres, a parte de tener un saborcito sensacional. $5.000";
        matriz[0][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/145207_MXM_K62728V1_OR1_FO_640x428.jpg";
        matriz[1][0] = "Licuado cremoso de sandia";
        matriz[1][1] = "Prueba este licuado cremoso de sandia hoy mismo. $6.000";
        matriz[1][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/135670_640x428.jpg";
        matriz[2][0] = "Mojito de fresa y menta";
        matriz[2][1] = "¿Buscas una bebida picara para brindar entre amigos? $8.000";
        matriz[2][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/115146_640x428.jpg";
        matriz[3][0] = "Granizado de durazno";
        matriz[3][1] = "Granizado de durazno como parte de un desayuno o almuerzo veraniego $5.000";
        matriz[3][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/550315_1_2_retail-4ddc6e88eece2fbfa0023e638fc6a4d049c5e33b_642x428.jpg";
        matriz[4][0] = "Daiquiri de fresa y sandia";
        matriz[4][1] = "Refrescate al maximo con esta deliciosa bebida de daiquiri de fresa y sandia. $6.000";
        matriz[4][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/164217_MXM_K64403v01_OR1_V_640x428.jpg";
        matriz[5][0] = "Licuado citrico de pepinos";
        matriz[5][1] = "Llenate de energía con lo mejor de las frutas citricas al tomar este delicioso licuado de pepinos $4.000";
        matriz[5][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/166461_MXM_K64796V0_OR1_CR_640x428.jpg";
        matriz[6][0] = "Cafe al caramelo";
        matriz[6][1] = "Este cafe al caramelo sera perfecto para disfrutar al maximo del cafecito entre amigas o de una sobremesa muy latina. $6.000";
        matriz[6][2] = "http://assets.kraftfoods.com/recipe_images/opendeploy/63448_640x428.jpg";
        siguiente();
    }
    void siguiente(){
        Intent sig = new Intent(this, mostrar.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("matriz", matriz);
        sig.putExtras(miBundle);
        startActivity(sig);
    }
}
